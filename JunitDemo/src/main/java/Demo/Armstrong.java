package Demo;

public class Armstrong {

	public boolean isArmstrong(int n) {
		if(n<100 || n>999)
			return false;
		else {
			int sum=0;
			
			int temp=n;
			
			while(temp!=0) {
				sum += Math.pow((temp%10), 3);
				temp /= 10;
			}
			
			if(sum==n)
				return true;
			else
				return false;
		}
	}
}
