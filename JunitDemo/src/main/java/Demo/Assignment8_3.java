package Demo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Assignment8_3 {
	
	static Armstrong obj;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		obj = new Armstrong();
		System.out.println("I am BeforeAll");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("I am AfterAll");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("I am BeforeEach");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("I am AfterEach");
	}
	
	@Test
	public void test1() {
		assertEquals(obj.isArmstrong(121), false);
		System.out.println("I am TestCase for isArmstrong(121)");
	}
	
	@Test
	public void test2() {
		assertEquals(obj.isArmstrong(153), true);
		System.out.println("I am TestCase for isArsmstrong(153)");
	}

}
